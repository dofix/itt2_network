## Last changed: 2019-02-21 11:18:47 UTC
version 12.1X46-D35.1;
system {
    host-name SRXA-2;
    root-authentication {
        encrypted-password "$1$B8qH7VAE$aNbJgwFMmNalgX3Cnpzm80"; ## SECRET-DATA
    }
    services {
        ssh;
        dhcp-local-server {
            group 11 {
                interface vlan.10;
            }
            group 4 {
                interface vlan.44;
            }
        }
        web-management {
            http {
                interface ge-0/0/1.0;
            }
        }
    }
    syslog {
        user * {
            any emergency;
        }
        file messages {
            any any;
            authorization info;
        }
        file interactive-commands {
            interactive-commands any;
        }
    }
    license {
        autoupdate {
            url https://ae1.juniper.net/junos/key_retrieval;
        }
    }
}
interfaces {
    ge-0/0/0 {
        unit 0;
    }
    ge-0/0/3 {
        unit 0 {
            family inet {
                address 10.217.19.239/22;
            }
        }
    }
    ge-0/0/4 {
        unit 0 {
            family inet {
                address 192.168.100.1/24;
            }
        }
    }
    ge-0/0/5 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-DMZ;
                }
            }
        }
    }
    ge-0/0/6 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-DMZ;
                }
            }
        }
    }
    ge-0/0/7 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-DMZ;
                }
            }
        }
    }
    ge-0/0/10 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-USERS;
                }
            }
        }
    }
    ge-0/0/11 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-USERS;
                }
            }
        }
    }
    ge-0/0/12 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-USERS;
                }
            }
        }
    }
    ge-0/0/13 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-USERS;
                }
            }
        }
    }
    vlan {
        unit 10 {
            family inet {
                address 192.168.11.1/24;
            }
        }
        unit 44 {
            family inet {
                filter {
                    input myfilter;
                    output myfilter;
                }
                address 192.168.4.1/24;
            }
        }
    }
}
forwarding-options {
    port-mirroring {
        input {
            rate 1;
            run-length 10;
        }
        family inet {
            output {
                interface ge-0/0/4.0 {
                    next-hop 192.168.100.50;
                }
            }
        }
    }
}
routing-options {
    static {
        route 0.0.0.0/0 next-hop 10.217.16.1;
    }
}
security {
    nat {
        destination {
            pool ApacheWebServer {
                address 192.168.4.10/32;
            }
            rule-set DestinationNATRuleWebServer {
                from zone untrust;
                rule ruleToWebServer {
                    match {
                        destination-address 10.217.19.239/32;
                        destination-port 80;
                    }
                    then {
                        destination-nat {
                            pool {
                                ApacheWebServer;
                            }
                        }
                    }
                }
            }
        }
    }
    policies {
        from-zone trust to-zone trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone trust to-zone untrust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone DMZ {
            /* policy for destination nat */
            policy WebServerPolicy {
                match {
                    source-address any;
                    /* WebServer from address book */
                    destination-address WebServer;
                    application junos-http;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone trust {
            interfaces {
                vlan.10 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                    }
                }
            }
        }
        security-zone untrust {
            interfaces {
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                    }
                }
            }
        }
        security-zone DMZ {
            address-book {
                address WebServer 192.168.4.10/32;
            }
            interfaces {
                vlan.44 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                        protocols {
                            all;
                        }
                    }
                }
                ge-0/0/4.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                        protocols {
                            all;
                        }
                    }
                }
            }
        }
    }
}
firewall {
    filter myfilter {
        term mirrorterm {
            from {
                source-address {
                    0.0.0.0/0;
                }
            }
            then {
                port-mirror;
                accept;
            }
        }
    }
}
access {
    address-assignment {
        pool 11 {
            family inet {
                network 192.168.11.0/24;
                range USERS {
                    low 192.168.11.2;
                    high 192.168.11.11;
                }
                dhcp-attributes {
                    router {
                        192.168.11.1;
                    }
                }
            }
        }
        pool 4 {
            family inet {
                network 192.168.4.0/24;
                range USERS {
                    low 192.168.4.25;
                    high 192.168.4.35;
                }
                dhcp-attributes {
                    router {
                        192.168.4.1;
                    }
                }
            }
        }
    }
}
vlans {
    vlan-DMZ {
        vlan-id 44;
        l3-interface vlan.44;
    }
    vlan-USERS {
        vlan-id 10;
        l3-interface vlan.10;
    }
}
